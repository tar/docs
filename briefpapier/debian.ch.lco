\ProvidesFile{debian.ch.lco}[2005/08/31 letter class option]

\LoadLetterOption{SN}

\usepackage{ifpdf,utf8,graphicx}

\setkomavar{fromname}{debian.ch}

\KOMAoptions{
  parskip=half,
  foldmarks=false,
  fromalign=left,
  fromlogo=true,
  fromrule=false,
  fromphone=false,
  fromemail=true,
  enlargefirstpage=true
}

\addtokomafont{subject}{\rmfamily}

\setkomavar{emailseparator}[]{}
\setkomavar{phoneseparator}[]{}

\renewcommand{\raggedsignature}{\raggedright}

\setkomavar{fromaddress}{%
  c/o Gaudenz Steinlin \\
  Donnerbühlweg 33 \\
  3012 Bern
}
\setkomavar{place}{Bern}
\setkomavar{fromemail}{info@debian.ch}
\ifpdf\setkomavar{fromlogo}{\includegraphics[scale=0.65]{debian-swirl.pdf}}
\else\setkomavar{fromlogo}{\includegraphics[scale=0.65]{debian-swirl.eps}}\fi

\setkomavar{frombank}{%
  Swiss Post - PostFinance, Nordring 8, 3030 CH-Bern / BIC: POFICHBEXXX \\
  Account 85-361360-7 / IBAN: CH12 0900 0000 8536 1360 7
}

\@addtoplength[-1]{firstfootvpos}{10ex}
\firstfoot{%
  \parbox[t]{\textwidth}{\footnotesize
    \begin{tabular}[t]{l@{}}%
      \multicolumn{1}{@{}l@{}}{Board:}\\
        Didier Raboud (President) \\
        Gaudenz Steinlin (Secretary) \\
        Philipp Hug (Treasurer) \\
      \multicolumn{1}{@{}l@{}}{Legal Domicile:}\\
        Bern, Switzerland
    \end{tabular}
    \hfill
    \begin{tabular}[t]{l@{}}%
      \multicolumn{1}{@{}l@{}}{Bank details:}\\
        \usekomavar{frombank} \\
        \\
      \multicolumn{1}{@{}l@{}}{Web presence:}\\
        http://www.debian.ch/
    \end{tabular}%
  }%
}%
