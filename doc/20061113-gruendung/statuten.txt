Statuten des Vereins debian.ch

Fassung vom 13.11.2006

§1 Einleitung
debian.ch ist ein Verein nach ZGB Art. 60ff mit Sitz in Zürich.

§2 Zweck
  1. debian.ch ist die offizielle Vertretung des Debian Projekts in
     der Schweiz und im Fürstentum Liechtenstein.
  2. Er hält im Namen des Debian Projekts Aktiven in der Schweiz und im
     Fürstentum Liechtenstein.
  3. Der Verein unterstützt das Debian Projekt durch seine Aktivitäten
     und Verwendung des Vereinsvermögens.

§3 Mittel
Der Verein finanziert sich durch Spenden und andere freiwilligen
Zuwendungen.  Es wird kein Mitgliederbeitrag erhoben.

§4 Mitgliedschaft
Der Vorstand entscheidet über die Aufnahme neür Mitglieder.

§5 Organe - Vollversammlung
  1. Der Vereinsvorstand lädt mindestens einmal jährlich zu einer
     Vollversammlung ein.  Die Einladung erfolgt mindestens zwei Wochen
     zum Voraus via E-Mail an alle Mitglieder.
  2. Die Einladungsfrist kann unterschritten werden wenn sämtliche
     Mitglieder zustimmen.
  3. Beschlüsse über Statutenänderungen können nicht sofort an der
     Versammlung in Kraft treten, an der sie beschlossen werden.

§6 Organe - Vorstand
  1. Der Vorstand besteht aus Präsident, Aktuar und Kassier.
     Ämterkumulation ist zulässig, jedoch muss das Amt des Präsidenten
     und des Kassiers von zwei verschiedenen Personen besetzt sein.
  2. Der Vorstand kann durch Beisitzer erweitert werden.
  3. Vorstände werden durch die Vereinsversammlung für ein
     Vereinsjahr gewählt.
  4. Der Vorstand konstituiert sich selbst.

§7 Vereinsjahr
Das Vereinsjahr entspricht dem Kalenderjahr.

§8 Auflösung
  1. Vereinsbeschluss über die Auflösung des Vereins erfordert eine
     Zweidrittelmehrheit der Jahresversammlung.  Es müssen ausserdem
     mindestens 50 oder, falls der Verein weniger als 66 Mitglieder hat,
     drei viertel aller Mitglieder anwesend sein.
  2. Bei Auflösung fliesst das Vereinsvermögen Software in the Public
     Interest, Inc., Indianapolis, USA zu.




Der Präsident     Martin F. Krafft


Der Aktuar        Adrian von Bidder


Der Kassier       Daniel Baumann
