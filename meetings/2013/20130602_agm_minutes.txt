-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Minutes
*******

Presence: Didier Raboud, Michael Germini, Luca Capello, Daniel Pocock, 
Axel Beckert, Eva Ramon, Philipp Hug, Gaudenz Steinlin (minutes)

* Greeting

  Voting members: 7

* Ratification of the minutes of the AGM 2012
  Agreed: 7
  No: 0

* Reports from the board:
 - report from the president
   Luca reports about the activities in 2012:
    - a new batch of knives was produced
    - most T-shirts are sold
    - we produced two banners
 - financial report
   Philipp presents the financial report, 
   see separate document
   Yes: 6
   Abstain: 1

* Discharge of the board
  Yes: 4
  Abstain: 3

* Members:
  - Eva Ramon, sponsored by Philipp Hug
    Yes: 7
    Now voting members: 8

* Board election:
  - elections for 2013, Candidates: 
    Gaudenz Steinlin, Philipp Hug, Didier Raboud
    Yes: 4
    No: 0
    Abstain: 4
  - We thank Luca for his work on the board for the last
    5 years.

* Bylaws:
  - translation to english, replacing the german 
    bylaws
  Yes: 8

  - change of the seat to Bern
  Yes: 8

* Merchandise
  - Merchandise activities in 2012
    see above report from the president
  - current stock
    the numbers were sent to the treasurer
  - future handling of merchandise
    - We agree that we want to keep the remainig merchandise
      for DebConf13 in Switzerland.
    - Unless someone want's to handle the logistics of selling
      and otherwise taking care, we don't produce any more 
      merchandise.

* Repoened the Members issue
  - new Rob Knights, sponsored by Daniel Pocock
    Yes: 8

* Varia
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIVAwUBUat/5M3PKyWkzVd5AQpIXQ/+LLxoIyulo0KXQS7AjXuNlCgL0vB7dZiH
LoAViOD6QOLBOWMmT6NVxODiDhZcyN1kIvOoNXgmMooQCUOKUGac9PGS9GmG80yf
oLNOCAJ65qJarY+BJvRZ0Ir2sQrj/jPrT5RQVT1e5KFMMPPPBcGanZaIaBnR2hb/
9BxLjpJAZb6H3TgpBlqCkOEw+eEMuxykzbY0Mz9kjsJXmqUc1x0F/2K/vZ+bM+/M
Z65M5EHopK1E8TqEHwVwpjMPwWMLMCGwVHAJ/Gc6tZV77MrOVL9wv5tI6ci8qX7Q
FLJyB4il5C6ITp8GpdhJJkC6yfu6Ylczt9ijQETfmvx+xTkRPGWxDLUSkJvosF3X
EUpy11xbMPKxqkri862FjSlhzSMMn5/es0bA0EPUgiyweXDwF5u5mpj0Ichq3/Q1
CoU4eXRE3HzsNg14MFxAWdNGKJ71DIW6cO6jgEXfgrzB4itsLL8hn9acM1pxJ2RZ
FuSW/kBVZzwtLuHyFd/+fPiJv9M+dhT7SapRnj6SCe0GXf7wBbyRYtZ88ULxiN+k
RlMvCn00wl/LBU/htPjxbYjOEDUODFfynVyxMhoEcTxADX4J9jJLyVwHElvmsMBh
YCXft2Da+mQfF5Te56ZkO+XUf/Yz0bfnWx3nVrT5VLD/ofKBWcG1BV7Wad87iu+l
y8+aoNgYAQ0=
=MV5k
-----END PGP SIGNATURE-----
